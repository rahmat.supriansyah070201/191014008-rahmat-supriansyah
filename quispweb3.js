const express = require('express');
const parser = require('body-parser');
const app = express();
const port = 5050;
const jsonBody = parser.json()

app.use(parser.json());

app.get('/get', (req, res) => {
  res.send('Belajar Web 3')
});

app.post("/post",jsonBody,function(req,res){
    var nama = req.body.nama;
    var kelas = req.body.kelas;
    res.send('Nama:' + nama  + ',' + kelas);
});

app.listen(port, () => {
  console.log(`cli-nodejs-api listening at http://localhost:${port}`)
});